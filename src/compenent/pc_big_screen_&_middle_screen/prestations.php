<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="prestation" id="prestation">
            <div class="prestationFrame">
                <div class="prestationFrameTitle">
                    <h1>Nos prestations et tarifs attractifs</h1>
                </div>
                <div class="prestationFrameContent">
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/prestations/application_mobile.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/prestations/platforme_logiciel.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/prestations/design_et_identie_visuelle.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/prestations/maintenance.php"); ?>
                </div>
                <div class="discoverMorePrestations">
                    <div class="discoverMorePrestationsCenter">
                        <div class="discoverMorePrestationsTitle">
                            <h1>Ce n'est pas tout !</h1>
                        </div>
                        <div class="discoverMorePrestationsDescription">
                            <p>Développer son application mobile ou web avec Kosmos Digital,<br />c'est s'entourer d'un panel d'experts ayant une vision globale de votre projet<br />et sachant activer les meilleurs leviers de réussite pour votre solution.</p>
                        </div>
                        <div class="discoverMorePrestationsButton">
                            <button>Découvrez toutes nos compétances</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>