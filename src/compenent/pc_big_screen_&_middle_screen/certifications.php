<div class="se-slope parallax pcView" id="methodID">
    <article class="se-content">
        <div class="certifications" id="certifications">
            <div class="certificationsFrame">
                <div class="certificationsTitle">
                    <h1>Nos développeurs sont certifiés</h1>
                </div>
                <div class="certificationsContent">
                    <div class="js-siema2">
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/5.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/7.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/8.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/4.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/6.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/9.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/18.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/10.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/11.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/3.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/12.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/13.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/17.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/14.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/1.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/15.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/16.png" alt="" />
                        </div>
                        <div class="owlFrameDiv">
                            <img src="https://www.kosmos-digital.com/media/certification/19.png" alt="" />
                        </div>
                    </div>
                    <div class="certificationFotter">
                        <p>Tous nos collaborateurs spécialisés sont certifiés dans leur domaine d'activité.</p>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>