<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="method" id="method">
            <div class="M_method">
                <div class="methodBg pcView"></div>
                <div class="method_text from-right">
                    <div class="method_textBig">
                        <h2 class="pcView">Une méthode de travail unique</h2> <!-- data-aos="fade-left" -->
                        <h2 class="mobileView">Une méthode de <br />travail unique</h2> <!-- data-aos="fade-left" -->
                        <div class="mobileView">
                            <img src="https://www.kosmos-digital.com/tmp/media/method.png" class="mobileUsMethodImage" />
                        </div>
                    </div>
                    <div class="method_subTitle mobileDisplayNone"> <!-- data-aos="fade-left" -->
                        <h2>Performance</h2>
                        <h2 class="colore6c340">+</h2>
                        <h2>Innovation</h2>
                        <h2 class="colore6c340 pcView">+</h2>
                        <h2 class="pcView">Smart price</h2>
                    </div>
                    <div class="method_content">
                        <p class="pcView">Nous utilisons les meilleurs outils, pricess et technologies pour votre projet.<br />Notre objectif est de garantir la longévité et l'adaptabilité de votre solution.<br />Toutes nos applications mobiles et platformes web son conçues de façons<br />à faciliter l'invervention d'équipes tierces et l'ensemble<br />des interconexions nécessaires.</p>
                        <p class="mobileView">Nous utilisons les meilleurs outils, pricess et technologies pour votre projet. Notre objectif est de garantir la longévité et l'adaptabilité de votre solution. Toutes nos applications mobiles et platformes web son conçues de façons à faciliter l'invervention d'équipes tierces et l'ensemble des interconexions nécessaires.</p>
                        <ul class="methodList">
                            <li><img src="https://www.kosmos-digital.com/tmp/media/check.png" /> <p>Acompagnement de A à Z multi-skill</p></li> <!-- data-aos="fade-left" -->
                            <li><img src="https://www.kosmos-digital.com/tmp/media/check.png" /> <p>Mise en place d'équipes spécialisées et sur mesure</p></li> <!-- data-aos="fade-left" -->
                            <li><img src="https://www.kosmos-digital.com/tmp/media/check.png" /> <p>Prise de décisions et directives centralisées</p></li> <!-- data-aos="fade-left" --><!-- data-aos="fade-left" -->
                            <li><img src="https://www.kosmos-digital.com/tmp/media/check.png" /> <p>Un seul interlocuteur par projet (account manager & project, dédié)</p></li> <!-- data-aos="fade-left" -->
                            <li><img src="https://www.kosmos-digital.com/tmp/media/check.png" /> <p>Solution Google friendly SEO/ SEA</p></li> <!-- data-aos="fade-left" -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="methodChiffre">
                <div class="methodChiffreFrame">
                    <div class="methodChiffreFrameNumber">
                        <h1>60</h1>
                    </div>
                    <div class="methodChiffreFrameText">
                        <p>coachs, développeurs et designers<br/>experts dans leurs domaine</p>
                    </div>
                </div>
                <div class="methodChiffreFrame">
                    <div class="methodChiffreFrameNumber">
                        <h1>+97 %</h1>
                    </div>
                    <div class="methodChiffreFrameText">
                        <p>de clients heureux et fidélisés<br/>prêt à poursuivre leur projet</p>
                    </div>
                </div>
                <div class="methodChiffreFrame">
                    <div class="methodChiffreFrameNumber">
                        <h1>+150</h1>
                    </div>
                    <div class="methodChiffreFrameText">
                        <p>applications créées par Kosmos<br/>et ses collaborateurs</p>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>