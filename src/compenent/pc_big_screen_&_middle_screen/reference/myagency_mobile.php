<a href="https://www.kosmos-digital.com/tmp/projets-kosmos.php?id=1">
    <div class="referenceBloc">
        <div class="referenceBlocImage">
            <div class="referenceBlocImage_img">
                <img src="https://www.kosmos-digital.com/media/ref/10.png" alt="reference" />
            </div>
            <div class="referenceBlocImage_icon">
                <div class="referenceBlocImage_BlocIcon"></div>
            </div>
            <div class="referenceBlocImage_arrow"></div>
        </div>
        <div class="referenceBlocContent">
            <div class="referenceBlocContentTStatusProjet">
                <div class="referenceBlocContentTStatusProjetLeft">
                    <h3>Pilot</h3>
                </div>
                <div class="referenceBlocContentTStatusProjetRight">
                    <h5>Projet en développement</h5>
                </div>
            </div>
            <div class="referenceBlocContentTStatusProjetDown">
                <p>Pilot Investment est une "Proptech". Une startup tournée vers les services immobiliers et plus précisément vers l'investissement locatif clef en main sur la Côte d'Azur. Kosmos Digital s'occupe intégralement du développement de ce projet : Site web, Espace client, Espace commerciaux, Application mobile native, Back-offices.</p>
            </div>
        </div>
    </div>
</a>