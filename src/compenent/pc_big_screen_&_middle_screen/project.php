<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="project" id="project">
            <div class="projectFrame">
                <div class="projectTitle">
                    <h1>Un projet ?</h1>
                </div>
                <div class="projectSubTitle">
                    <h2>Nos équipes répondent à toutes vos questions</h2>
                </div>
                <div class="projectContent">
                    <button>Contactez-nous</button>
                </div>
            </div>
        </div>
    </article>
</div>