<div class="footer" id="footer">
    <div class="footerFrame">
        <div class="footerFrameLeft">
            <div class="footerTitle">
                <h1>Ensemble, réalisons votre projet !</h1>
                <div class="footerStepBis">
                    <h3>Distinguez-vous sur les stores !</h3>
                </div>
            </div>
            <div class="footerList">
                <form method="POST" action="">
                    <div class="formBox">
                        <div class="formBoxLabel">
                            <p>Votre nom ou société*</p>
                        </div>
                        <div class="formBoxInput">
                            <input type="text" class="inp" placeholder="John doe" />
                        </div>
                    </div>
                    <div class="formBox">
                        <div class="formBoxLabel">
                            <p>Une adresse e-mail de contact*</p>
                        </div>
                        <div class="formBoxInput">
                            <input type="text" class="inp" placeholder="john.doe@kosmos-digital.com" />
                        </div>
                    </div>
                    <div class="formBox">
                        <div class="formBoxLabel">
                            <p>Numéro de téléphone*</p>
                        </div>
                        <div class="formBoxInput">
                            <input type="text" class="inp" placeholder="+3376838xxxx" />
                        </div>
                    </div>
                    <div class="formBox">
                        <div class="formBoxLabel">
                            <p>Votre message</p>
                        </div>
                        <div class="formBoxInput">
                            <textarea type="text">Bonjour, l'un de vos services m'intéresse. Merci de me recontacter au plus vite afin que l'on puisse en discuter. Cordialement.</textarea>
                        </div>
                    </div>
                    <div class="formBox">
                        <div class="formBoxInput">
                            <div class="formBoxDashed">
                                <input type="file" multiple="" class="inpFile" />
                                <p>Cliquer pour sélectrionner votre fichier</p>
                            </div>
                        </div>
                    </div>
                    <div class="formBox">
                        <div class="formBoxInput">
                            <button>Envoyer votre message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="footerFrameRight">
            <div class="footerTitle">
                <h3>Nous acceptons les projets situés partout<br /> en France, Suisse, Belgique, Luxembourg,<br /> Canada*</h3>
            </div>
            <div class="footerStep">
                <h2>0980801383</h2>
                <div class="footerStepBis">
                    <h5>N° non surtaxé. Prix d'un appel.</h5>
                </div>
            </div>
            <div class="footerStep">
                <p>Appelez-nous de 9h à 20h du lundi au samedi.</p>
                <p>hello@kosmos-digital.com</p>
                <p>Siège : 3 Place Masséna - 06000 Nice</p>
            </div>
            <div class="footerStep">
                <p>
                    Paris • 14, Rue des Reculettes<br/>
                    Marseille • 48 Quai du Lazaret<br/>
                    Lyon • 26 Rue Maurice Flandin<br/>
                    Toulouse • 32 Rue Riquet<br/>
                    Nantes • 11 Rue La Noue Bras de Fer<br/>
                    Montpellier • 2 Rue du Pavillon<br/>
                    Strasbourg • 204 Avenue de Colmar<br/>
                    Bordeaux • 132 Rue Fondaudège<br/>
                    Lille • Arteparc, 9 Rue des Bouleaux
                </p>
            </div>
        </div>
    </div>
</div>