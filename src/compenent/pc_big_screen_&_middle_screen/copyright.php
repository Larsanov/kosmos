<div class="copyright">
    <div class="copyrightFrame">
        <div class="copyrightBloc">
            <img src="https://www.kosmos-digital.com/tmp/media/utils/logoMenu.png" width="161.5" height="42.5" alt="logo kosmos" />
        </div>
        <div class="copyrightBloc">
            <div class="copyrightBlocLineStart">
                <div>Excellent 9,7/10</div>
                <div></div>
            </div>
            <div class="copyrightBlocLine">
                <div>© Kosmos</div>
                <div><a href="https://www.kosmos-digital.com/tmp/mention-legal.php">Politique de confidentialité</a></div>
                <div><a href="https://www.kosmos-digital.com/tmp/mention-legal.php">Mentions légales</a></div>
            </div>
        </div>
        <div class="copyrightBloc">
            <p class="pcView">Kosmos digital est une marque déposée. Son icône symbolise une planète et est une marque enregistrée. L'ensemble des produits et prestations sont émis par Kosmos digital.<br/>Conformément à la Directive 2006/112/CE modifiée, à partir du 01/01/2015, les prix TTC sont susceptibles de varier selon le pays de résidence du client (par défaut les prix TTC affichés incluent la TVA française en vigueur).</p>
            <p class="mobileView">Kosmos digital est une marque déposée. Son icône symbolise une planète et est une marque enregistrée. L'ensemble des produits et prestations sont émis par Kosmos digital. Conformément à la Directive 2006/112/CE modifiée, à partir du 01/01/2015, les prix TTC sont susceptibles de varier selon le pays de résidence du client (par défaut les prix TTC affichés incluent la TVA française en vigueur).</p>
        </div>
    </div>
</div>