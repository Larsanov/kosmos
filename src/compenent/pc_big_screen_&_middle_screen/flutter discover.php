<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="discover" id="discover">
            <div class="discoverFrame">
                <div class="discoverFrameTitle">
                    <h1>Découvrez pourquoi nous avons fait<br />de Flutter notre spécialité sur mobile !</h1>
                    <button>Contactez-nous</button>
                </div>
                <div class="discoverFrameImage">
                    <img src="https://www.kosmos-digital.com/tmp/media/flutter.png" alt="logo flutter" />
                </div>
            </div>
        </div>
    </article>
</div>