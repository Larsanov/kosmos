
<div class="why" id="why">
    <div class="W_why">
        <div class="whyCoun">
            <div class="whyFrame shadowWhyFrame transition3" id="whyFrame_a"> <!-- data-aos="slide-up" -->
                <div class="whyFrameNumber">
                    <h1 style="color: #022C93;">1</h1>
                </div>
                <div class="whyFrameTitle whyFrameTitle1">
                    <h2>30 % d'économies</h2>
                </div>
                <div class="whyFrameContent">
                    <p>Notre agence est principalement digitalisée ce qui vous permet d'économiser en moyenne plus de 30% sur le coût de votre projet par rapport à une agence web/ mobile traditionelle ou des freelances isolés, à qualité et prestation égales. Prix bas garantis.</p>
                </div>
            </div>
            <div class="whyFrame shadowWhyFrame transition6" id="whyFrame_b"> <!-- data-aos="slide-up" -->
                <div class="whyFrameNumber">
                    <h1 style="color: #00b6ff;">2</h1>
                </div>
                <div class="whyFrameTitle whyFrameTitle2">
                    <h2>Rapidité et efficacité</h2>
                </div>
                <div class="whyFrameContent">
                    <p>Nous mettons en place une équipe adéquate et complète, dédiée à 100% à votre projet. Chaque équipe est rodée et travaille projet par projet pour plus d’efficacité et de rapidité. Délais courts garantis.</p>
                </div>
            </div>
            <div class="whyFrame shadowWhyFrame transition9 kljgkjnkdnfkfkjsdsf" id="whyFrame_c"> <!-- data-aos="slide-up" -->
                <div class="whyFrameNumber">
                    <h1 style="color: #e62b7c;">3</h1>
                </div>
                <div class="whyFrameTitle whyFrameTitle3">
                    <h2>Qualité exceptionnelle</h2>
                </div>
                <div class="whyFrameContent">
                    <p>Nous travaillons avec plus de 60 collaborateurs multidisciplinaires. Tous sont professionnels diplômés et triés sur le volet, afin d'obtenir des résultats époustouflants, durables et efficaces.</p>
                </div>
            </div>
        </div>
    </div>
</div>