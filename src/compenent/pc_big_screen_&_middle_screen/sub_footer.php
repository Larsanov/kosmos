<div class="subFooter">
    <div class="subFooterFrame">
        <div class="subFooterBloc">
            <div class="subFooterBlocTitle">
                <h3>Nos solutions</h3>
            </div>
            <div class="subFooterBlocContent">
                <ul>
                    <li>Pourquoi choisir Kosmos ?</li>
                    <li>Nos prestations et tarifs attractifs</li>
                    <li>Nos références actuelles</li>
                </ul>
            </div>
        </div>
        <div class="subFooterBloc">
            <div class="subFooterBlocTitle">
                <h3>Communauté</h3>
            </div>
            <div class="subFooterBlocContent">
                <ul>
                    <li>Ils nous font confiance</li>
                    <li>Votre stratégie offerte</li>
                    <li>Nous contacter</li>
                </ul>
            </div>
        </div>
        <div class="subFooterBloc">
            <div class="subFooterBlocTitle">
                <h3>Kosmos Recrute !</h3>
            </div>
            <div class="subFooterBlocContent">
                <ul>
                    <li>Apporteurs d'affaires</li>
                    <li>Agents commerciaux</li>
                    <li>Développeurs front-end</li>
                    <li>Développeurs back-end</li>
                    <li>Développeurs fullstack</li>
                    <li>Graphistes</li>
                    <li>Designers web</li>
                    <li>Chefs de projets</li>
                </ul>
            </div>
        </div>
        <div class="subFooterBloc">
            <div class="subFooterBlocTitle">
                <h3>Nous suivre</h3>
            </div>
            <div class="subFooterBlocContent">
                <form method="POST" action="">
                    <input type="email" class="inpSubFooter" placeholder="Votre email" />
                    <button>Inscription newsletter</button>
                </form>
                <div class="subFooterBlocContentBis">
                    <p>Conformément à la loi « Informatique et libertés » du 6 janvier 1978 modifiée en 2004 vous bénéficiez d'un droit d'accès et de rectification des informations qui vous concernent.</p>
                </div>
            </div>
        </div>
    </div>
</div>