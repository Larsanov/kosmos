<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="runSolution" id="runSolution">
            <div class="runSolutionFrame">
                <div class="runSolutionFrameTitle">
                    <h1 class="pcView">Quelque soit votre cible et vos objectifs, nous<br />saurons vous accompagner de la conception<br />jusqu'au déploiement de votre solution.</h1>
                    <h1 class="mobileView">Quelque soit votre cible et vos objectifs, nous saurons vous accompagner de la conception jusqu'au déploiement de votre solution.</h1>
                    <button onclick="popUpContact();">Lancez-votre solution. Contactez-nous</button>
                </div>
            </div>
        </div>
    </article>
</div>