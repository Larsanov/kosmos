<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="reference" id="reference">
            <div class="referenceFrame">
                <div class="referenceTitle">
                    <h1>Quelques une de nos références fdp</h1>
                </div>
                <div class="referenceContent">
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/telpro_pc.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/telpro_mobile.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/pilot_pc.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/pilot_mobile.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/myagency_pc.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/myagency_mobile.php"); ?>
                    <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference/plus.php"); ?>
                </div>
            </div>
        </div>
    </article>
</div>