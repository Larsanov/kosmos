<div class="prestationBloc" onclick="popUpPrestation(3, '#e62b7c');">
    <!-- Icon -->
    <div class="prestationBlocIcon">
        <div class="prestationBlocIconCorner background62b7c">
            <img src="https://www.kosmos-digital.com/media/bigicon/maintenance.png" />
        </div>
    </div>
    <!-- Title -->
    <div class="prestationBlocTitle">
        <h3>Maintenance</h3>
    </div>
    <!-- Price -->
    <div class="prestationBlocPrice">
        <h5>à partir de 49 €/mois</h5>
    </div>
    <!-- Content -->
    <div class="prestationBlocContent">
        <p>Nous gérons la maintenance de votre site et de votre application mobile : Mises à jour, sauvegardes, sécurité, performances, nettoyages, firewall, surveillance, rapport détaillé, restauration, transfert. Notre équipe d'account managers dédiés est garante de...</p>
    </div>
</div>