<div class="prestationBloc" onclick="popUpPrestation(31, '#00b6ff');">
    <!-- Icon -->
    <div class="prestationBlocIcon">
        <div class="prestationBlocIconCorner background00b6ff">
            <img src="https://www.kosmos-digital.com/media/bigicon/software.png" />
        </div>
    </div>
    <!-- Title -->
    <div class="prestationBlocTitle">
        <h3>Platforme / Web</h3>
    </div>
    <!-- Price -->
    <div class="prestationBlocPrice">
        <h5>à partir de 4900 €</h5>
    </div>
    <!-- Content -->
    <div class="prestationBlocContent">
        <p>Nous développons des logiciels téléchargeables (on premise) et des logiciels en ligne (SAAS – Software as a Service) gratuits (graticiels) ou payants (abonnement ou licence). Tous sont réalisés sur mesure, sont complets, utilisables sur l’ensemble des supports de votre choix...</p>
    </div>
</div>