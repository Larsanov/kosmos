<div class="prestationBloc" onclick="popUpPrestation(2, '#022C93');">
    <!-- Icon -->
    <div class="prestationBlocIcon">
        <div class="prestationBlocIconCorner background022C93">
            <img src="https://www.kosmos-digital.com/media/bigicon/app.png" />
        </div>
    </div>
    <!-- Title -->
    <div class="prestationBlocTitle">
        <h3>Application mobile</h3>
    </div>
    <!-- Price -->
    <div class="prestationBlocPrice">
        <h5>à partir de 4900 €</h5>
    </div>
    <!-- Content -->
    <div class="prestationBlocContent">
        <p>Nous développons des applications mobiles sur mesure, complètes et sécurisées en tenant compte des enjeux actuels et selon les meilleurs standards : natives, hybrides, Web Apps. Nous effectuons également la refonte, l’optimisation et la réparation de votre application mobile...</p>
    </div>
</div>