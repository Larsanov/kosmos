<div class="prestationBloc" onclick="popUpPrestation(4, '#e62b7c');">
    <!-- Icon -->
    <div class="prestationBlocIcon">
        <div class="prestationBlocIconCorner background62b7c">
            <img src="https://www.kosmos-digital.com/media/bigicon/design.png" />
        </div>
    </div>
    <!-- Title -->
    <div class="prestationBlocTitle">
        <h3>Design et identité visuelle</h3>
    </div>
    <!-- Price -->
    <div class="prestationBlocPrice">
        <h5>à partir de 49 €</h5>
    </div>
    <!-- Content -->
    <div class="prestationBlocContent">
        <p>Nous créons des interfaces graphiques ergonomiques et intelligentes sur mesure, afin d’augmenter la conversion grâce à une disposition et un aspect optimisant le Call to Action. Nous sommes en mesure de présenter de manière simple les choses les plus...</p>
    </div>
</div>