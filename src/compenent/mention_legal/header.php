<div class="mentionLegal">
    <div class="mentionLegalFrame">
        <div class="mentionLegalFrameTitle">
            <h1>Mentions légales et confidentialité</h1>
        </div>
        <div class="mentionLegalFrameContent">
            <div class="mentionLegalFrameContentParagraphe">
                <p>Conformément aux dispositions de la loi n° 2004-575 du 21 juin 2004 pour la confiance en l'économie numérique, il est précisé aux utilisateurs du site l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi.</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Edition du site</strong>
            </div>

            <div class="mentionLegalFrameContentParagraphe">
                <p>Le site www.kosmos-digital.com est édité par la société CALCULIMMO au capital social de 7.000 euros immatriculée au RCS de Nice, ayant son siège social 3, Place Masséna 06000 NICE, comme numéro SIRET 838 447 449 00029 et numéro de TVA intracommunautaire FR51838447449</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Responsable de la publication</strong>
            </div>

            <div class="mentionLegalFrameContentParagraphe">
                <p>La responsable de la publication est Anna GOLUBEVA, présidente de la société CALCULIMMO.</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Contenu</strong>
            </div>

            <div class="mentionLegalFrameContentParagraphe">
                <p>Les informations mises en ligne font l'objet de mises à jour régulières et peuvent être modifiées à tout moment. L'utilisateur en est conscient et se doit de vérifier les informations recueillies. CALCULIMMO décline toute responsabilité quant au contenu du site et à son utilisation. Au cas où des liens seraient créés avec d'autres sites, CALCULIMMO ne peut en aucun cas être tenu responsable du contenu et de l'existence de ces autres liens. Les données contenues sur le site www.kosmos-digital.com sont protégées par la loi du 1er juillet 1998 sur les bases de données. En accédant ou en utilisant le site, vous reconnaissez vous conformer aux dispositions de la loi, et notamment en s'interdisant l'extraction, le transfert, le stockage, la reproduction de tout ou partie qualitativement ou quantitativement substantielle du contenu des bases de données figurant sur le site. La reproduction, la rediffusion ou l'extraction automatique par tout moyen d'informations figurant sur www.kosmos-digital.com est interdite. L'emploi de robots, programmes permettant l'extraction directe de données est rigoureusement interdit.</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Droits de propriété et contrefaçons</strong>
            </div>

            <div class="mentionLegalFrameContentParagraphe">
                <p>CALCULIMMO détient, se réserve et conserve tous les droits de propriété, notamment intellectuelle, y compris les droits de reproduction sur le présent site et les éléments qu’il contient. En conséquence et notamment toute reproduction partielle ou totale du présent site et des éléments qu'il contient est strictement interdite sans autorisation écrite de CALCULIMMO. Les logos et tous autres signes distinctifs contenus sur ce site sont la propriété de CALCULIMMO ou font l'objet d'une autorisation d'utilisation. Aucun droit ou licence ne saurait être attribué sur l'un quelconque de ces éléments sans l'autorisation écrite de CALCULIMMO ou du tiers détenteur des droits. Tous les textes, illustrations et images présents sur le site www.kosmos-digital.com sont soumis au titre du droit d'auteur ainsi qu'au titre de la propriété intellectuelle. Il est interdit de copier, reproduire, diffuser, vendre, publier, exploiter et diffuser les informations présentes sur le site www.kosmos-digital.com. En conséquence, toute autre utilisation que la consultation est constitutive de contrefaçon et sanctionnée au titre de la propriété intellectuelle. Selon les termes de l'article L.122-4 du Code de la Propriété Intellectuelle, « toute représentation ou reproduction intégrale ou partielle faite sans le consentement de l'auteur est illicite ». Par conséquent, la consultation ou modification de la base de données au moyen de programmes ou applications autres que ceux mis à disposition sur www.kosmos-digital.com est interdit. L'article L335-3 du CPI dispose en outre qu'est « un délit de contrefaçon la violation de l'un des droits de l'auteur de logiciel ». Toute personne ne respectant pas ces dispositions légales s'exposerait aux peines prévues à l'article L335-2 du même code (jusqu'à cinq ans d'emprisonnement et 15 000 € d'amende), ainsi qu'à une condamnation au paiement de dommages intérêts dans le cadre d'assignations devant les juridictions civiles. CALCULIMMO dispose d'un service juridique compétent qui n'hésitera pas à déclencher les poursuites nécessaires pour faire valoir ce que de droit.</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Limitations de responsabilité.</strong>
            </div>

            <div class="mentionLegalFrameContentParagraphe">
                <p>CALCULIMMO ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site de l’agence web www.kosmos-digital.com. Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont à la disposition des utilisateurs. CALCULIMMO se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, CALCULIMMO se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie).</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Nous contacter</strong>
            </div>

            <div class="mentionLegalFrameContentParagraphe">
                <p>Par téléphone : +33 9 80 80 13 83</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <p>Par email : hello@kosmos-digital.com</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <p>Par courrier : CALCULIMMO, 3 Place Masséna 06000 NICE,</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <strong>Hébergeur</strong>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <p>Le site www.kosomos-digital.com est hébergé par la société OVH SAS.</p>
            </div>
            <div class="mentionLegalFrameContentParagraphe">
                <p>Le stockage des données personnelles des utilisateurs est exclusivement réalisé sur les centres de données ("clusters") de la société OVH SAS (filiale de la société OVH Groupe SAS, société immatriculée au RCS de Lille sous le numéro 537 407 926 sise 2, rue Kellermann, 59100 Roubaix). Tous les clusters OVH sur lesquels les données du Site sont stockées sont localisés dans des Etats membres de l'Union Européenne.</p>
            </div>
        </div>
    </div>
</div>