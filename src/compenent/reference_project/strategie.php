<div class="se-slope parallax" id="methodID">
    <article class="se-content">
        <div class="bodyProject">
            <div class="StategieProjectFrame">
                <div class="strategieBlocKosmosProject">
                    <div class="strategieBlocKosmosProjectTitle">
                        <h2>Design</h2>
                    </div>
                    <div class="strategieBlocKosmosProjectContent"></div>
                </div>
                <div class="strategieBlocKosmosProject">
                    <div class="strategieBlocKosmosProjectTitle">
                        <h2>Developpements</h2>
                    </div>
                    <div class="strategieBlocKosmosProjectContent"></div>
                </div>
                <div class="strategieBlocKosmosProject">
                    <div class="strategieBlocKosmosProjectTitle">
                        <h2>Strategies</h2>
                    </div>
                    <div class="strategieBlocKosmosProjectContent"></div>
                </div>
            </div>
        </div>
    </article>
</div>