<div class="all lazy" id="all">
    <?php // include("./src/utils/cookies.php"); ?>
    <?php // include("./src/utils/info_window.php"); ?>

    <!-- PC big size screen, middle size screen and very small screen size pc minimum pixel : >= 950 -->
    <!-- <div class="pcView"> -->
        <?php include("./src/compenent/pc_big_screen_&_middle_screen/grey_bloc.php"); ?>
        <?php include("./src/compenent/pc_big_screen_&_middle_screen/menu.php"); ?>
        <?php include("./src/compenent/pc_big_screen_&_middle_screen/header.php"); ?>

        <section class="se-container" id="allSection">
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/our_method.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/partner.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/flutter_discover.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/yellow_bloc.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/prestations.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/reference.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/project.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/certifications.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/footer.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/sub_footer.php"); ?>
            <?php include("./src/compenent/pc_big_screen_&_middle_screen/copyright.php"); ?>
        </section>
    <!-- </div> -->
</div>