<div class="popUp" id="popUpContact">
    <div class="centerPopUp animated bounceInUp2">
        <div class="contentPopUP" id="contentPopUP">
            <div class="contentPopUPBloc" style="padding-top: 40px; display: flex; flex-direction: row; justify-content: space-between;">
                <h1>Contactez-nous !</h1>
                <div class="closePopUP">
                    <img src="https://www.kosmos-digital.com/tmp/media/cr.png" width="20" height="20" id="closePopUp" />
                </div>
            </div>
            <div class="contentPopUPForm">
                <div class="contentPopUPBloc">
                    <div class="formBoxLabel">
                        <p>Votre nom ou société*</p>
                    </div>
                    <div class="formBoxInput">
                        <input type="text" class="inp" placeholder="john doe" />
                    </div>
                </div>
                <div class="contentPopUPBloc">
                    <div class="formBoxLabel">
                        <p>Une adresse e-mail de contact*</p>
                    </div>
                    <div class="formBoxInput">
                        <input type="email" class="inp" placeholder="john.doe@gmail.com" />
                    </div>
                </div>
                <div class="contentPopUPBloc">
                    <div class="formBoxLabel">
                        <p>Votre numéro de téléphone*</p>
                    </div>
                    <div class="formBoxInput">
                        <input type="email" class="inp" placeholder="076838xxxx" />
                    </div>
                </div>
                <div class="contentPopUPBloc">
                    <div class="formBoxLabel">
                        <p>Votre message</p>
                    </div>
                    <div class="formBoxInput">
                        <textarea type="text">Bonjour, l'un de vos services m'intéresse. Merci de me recontacter au plus vite afin que l'on puisse en discuter. Cordialement.</textarea>
                    </div>
                </div>
                <div class="contentPopUPBloc">
                    <div class="formBoxInput">
                        <div class="formBoxDashed">
                            <input type="file" multiple="" class="inpFile" />
                            <p>Cliquer pour sélectrionner votre fichier</p>
                        </div>
                    </div>
                </div>
                <div class="contentPopUPBloc" style="padding-bottom: 40px;">
                    <div class="formBoxInput">
                        <button>Envoyer votre message</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>