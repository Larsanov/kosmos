<div class="info_window" id="info_window">
    <div class="info_windowHead" id="info_windowHead" onclick="info_window_open();">
        <div class="info_windowHeadTitle">
            <h2>Estimer votre projet</h2>
        </div>
        <div class="info_windowHeadTitle">
            <h5>Gratuit | Stratégie offerte | Prix bas garantis</h5>
        </div>
    </div>
    <div class="info_windowContent">
        <div class="info_windowContentBloc">
            <img src="https://www.kosmos-digital.com/media/baptiste.png" style="border-radius: 100px;" />
        </div>
        <div class="info_windowContentBloc">
            <h2>Appelez-nous !</h2>
        </div>
        <div class="info_windowContentBloc">
            <h1>09 80 80 13 83</h1>
        </div>
        <div class="info_windowContentBloc">
            <p>du lundi au samedi de 9h à 20h<br/>N° non surtaxé</p>
        </div>
        <div class="info_windowContentBloc">
            <button onclick="popUpCall();">Être rappelé</button>
        </div>
        <div class="info_windowContentBloc">
            <button onclick="popUpContact();">Nous contacter</button>
        </div>
    </div>
</div>