setInfoWindow();

function setInfoWindow() {
    var info_window = document.getElementById("info_window").style;

    if ($(window).width() >= 950) {
        info_window.bottom = "30px";
        info_window_close();
    }
    if ($(window).width() < 950) {
        info_window.bottom = "-370px";
        info_window_open();
    }
}

window.addEventListener("resize", setInfoWindow);

scrollAction();

function scrollAction() {
    var scroll = this.scrollY;
    var menu = document.getElementById("menu").style;
    var info_window = document.getElementById("info_window").style;

    if ($(window).width() >= 950) {
        if (scroll > 0) {
            menu.position = "fixed";
            menu.top = "0px";
            menu.boxShadow = "0 2px 10px 0 rgba(0, 0, 0, 0.15)";
        }
        if (scroll == 0) {
            menu.position = "fixed";
            menu.top = "60px";
            menu.boxShadow = "0px 0px 0px 0px #000";
        }
        if (scroll >= 100) {
            info_window.display = "block";
        }
        if (scroll < 100) {
            info_window.display = "none";
        }
    }
    if ($(window).width() < 950) {
        info_window.display = "block";
        if (scroll > 0) {
            menu.position = "fixed";
            menu.top = "0px";
            menu.boxShadow = "0 2px 10px 0 rgba(0, 0, 0, 0.15)";
        }
        if (scroll == 0) {
            menu.position = "fixed";
            menu.top = "0px";
            menu.boxShadow = "0px 0px 0px 0px #000";
        }
    }
}

window.addEventListener("scroll", scrollAction);
window.addEventListener("resize", scrollAction);

function popUpContact() {
    document.getElementById("popUpContact").style.display = "block";
    document.body.style.overflow = 'hidden';
}

$('#popUpContact').click(function(event) {
    if (!document.getElementById('contentPopUP').contains(event.target)) {
        document.getElementById("popUpContact").style.display = "none";
        document.body.style.overflowY = "scroll";
    }
});

$("#closePopUp").click(function(event) {
    document.getElementById("popUpContact").style.display = "none";
    document.body.style.overflowY = "scroll";
});

/********************************************/

function popUpCall() {
    document.getElementById("popUpCall").style.display = "block";
    document.body.style.overflow = 'hidden';
}

$("#popUpCall").click(function(event) {
    if (!document.getElementById('contentPopUPCall').contains(event.target)) {
        document.getElementById("popUpCall").style.display = "none";
        document.body.style.overflowY = "scroll";
    }
});

$("#closePopUpCall").click(function(event) {
    document.getElementById("popUpCall").style.display = "none";
    document.body.style.overflowY = "scroll";
});

/********************************************/

function popUpPrestation(value, color) {
    var size = $("#prestationPopUpBlocContent").height();
    var imgPrestation = "i" + value;
    var titlePrestation = "t" + value;
    var pricePrestation = "p" + value;
    var contentPrestation = "c" + value;

    document.getElementById("popUpPrestation").style.display = "block";

    for (var i = 1; i <= 33; i += 1) {
        var otherImg = "i" + i;
        var otherTitle = "t" + i;
        var otherPrestation = "p" + i;
        var otherContent = "c" + i;

        if (i == value) {
            document.getElementById("prestationColorBloc").style.background = color;
            document.getElementById(imgPrestation).style.display = "block";
            document.getElementById(titlePrestation).style.display = "block";
            document.getElementById(pricePrestation).style.display = "block";
            document.getElementById(contentPrestation).style.display = "block";
        } else {
            document.getElementById(otherImg).style.display = "none";
            document.getElementById(otherTitle).style.display = "none";
            document.getElementById(otherPrestation).style.display = "none";
            document.getElementById(otherContent).style.display = "none";
        }
    }
    if ($(window).width() >= 950) {
        if (size >= 550) {
            $("#prestationPopUpBlocContent").scrollTo(0, 0);
            document.getElementById("prestationPopUpBlocContent").style.overflowY = "scroll";
        }
    }
    document.body.style.overflow = 'hidden';
}

$("#popUpPrestation").click(function(event) {
    if (!document.getElementById('contentPopUPPrestation').contains(event.target)) {
        document.getElementById("popUpPrestation").style.display = "none";
        document.body.style.overflowY = "scroll";
    }
});

$("#closePopUpPrestation").click(function(event) {
    document.getElementById("popUpPrestation").style.display = "none";
    document.body.style.overflowY = "scroll";
});

function info_window_open() {
    var elem = document.getElementById("info_window");
    var pos = -370;

    elem.style.transition = "1s";
    elem.style.bottom = pos + 'px';
    document.getElementById("info_windowHead").setAttribute( "onClick", "javascript: info_window_close();");
}

function info_window_close() {
    var elem = document.getElementById("info_window");
    var pos = 0;

    elem.style.transition = "1s";
    elem.style.bottom = pos + 'px';
    document.getElementById("info_windowHead").setAttribute( "onClick", "javascript: info_window_open();");
}