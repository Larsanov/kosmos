<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no"/>
        <meta name="description" content="Nous développons de superbes sites internet, applications mobiles et logiciels en vous accompagnant dans votre projet de A à Z, jusqu'au lancement et bien après. Nous réalisons également le marketing ainsi que l'ensemble de la publicité digitale de votre projet. Contactez-nous afin que nous estimions votre projet. Nous sommes en moyenne 30% moins chers que la concurrence." />
        <link rel="stylesheet" type="text/css" href="./style/f.css" media="screen and (min-width: 1455px)" />
        <link rel="stylesheet" type="text/css" href="./style/pc.css" media="screen and (min-width: 1455px)" />
        <link rel="stylesheet" type="text/css" href="./style/middle.css" media="screen and (min-width: 950px) and (max-width: 1455px)" />
        <link rel="stylesheet" type="text/css" href="./style/mobile.css" media="screen and (min-width: 0px) and (max-width: 950px)" />
        <link rel="stylesheet" type="text/css" href="./style/mobile.css" media="screen and (min-device-width: 0px) and (max-device-width: 950px)" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Google+Sans:400|Product+Sans:400|Roboto:100,300,400,500,700&lang=fr" rel="stylesheet" nonce="LVNmPG0GKXnXPe67Wp04Rg">
        <script src="https://www.googletagmanager.com/gtag/js?id=AW-735360116"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'AW-735360116');
        </script>
        <script>
            gtag('config', 'AW-735360116/Ze3LCIag0bUBEPTo0t4C', {
                'phone_conversion_number': '0980801383'
            });
        </script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154163099-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-154163099-1');
        </script>
        <meta http-equiv="X-UA-Compatible" content="IE=8,IE=9" />
        <meta charset="UTF-8" />
        <title>Kosmos Digital - Agence digitale 360 ° - Création de Sites web, Applications mobiles, Marketing digital, Publicité digitale et Référencement web</title>
        <link rel="icon" type="image/png" href="https://www.kosmos-digital.com/tmp/media/logob.png" />
        <script src="./javascript/jquery.js"></script>
    </head>
    <body>
        <?php include("./src/body/body.php"); ?>
        <?php include("./src/utils/popup.php"); ?>
        <?php include("./src/utils/info_window.php"); ?>
        <script src="./javascript/my.js"></script>
        <script src="https://www.kosmos-digital.com/tmp/javascript/siema.js"></script>
        <script>
            const mySiema = new Siema({
                selector: '.js-siema2',
                loop: true,
                perPage: 5,
            });
        </script>
    </body>
</html>